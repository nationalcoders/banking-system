package com.national.corporation.controller;

import com.national.corporation.entity.User;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/bs/v1/login")
public class LoginController {

    @ApiOperation(value = "login", nickname = "login")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
         @ApiImplicitParam(name = "User", value =  "com.national.corporation.entity.User", required = true, dataType = "com.national.corporation.entity.User")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User authenticated successfully"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    public ResponseEntity<HttpStatus> login(@RequestBody User user) {
        if (null == user) {
            return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else if ("Pratap".equalsIgnoreCase(user.getFirstName())) {
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } else {
           return  new ResponseEntity<HttpStatus>(HttpStatus.UNAUTHORIZED);
        }
    }
}
