package com.national.corporation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

import com.national.corporation.entity.Address;
import com.national.corporation.entity.Permissions;
import com.national.corporation.entity.RolePermisson;
import com.national.corporation.entity.Roles;
import com.national.corporation.entity.User;
import com.national.corporation.entity.UserDetails;
import com.national.corporation.repository.PermissionRepository;
import com.national.corporation.repository.RolePermissionRepository;
import com.national.corporation.repository.RolesRepository;
import com.national.corporation.repository.UserRepository;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@SpringBootApplication
@EnableSwagger2
@ImportResource(value = {"classpath:spring/resource.xml"})
public class Application {
	
	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);		
	}

	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("National-Bank")
				.apiInfo(apiInfo())
				.select()
				.paths(regex("/rest.*"))
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Banking System with Swagger")
				.description("Banking System with Swagger")
				.termsOfServiceUrl("http://www-03.ibm.com/software/sla/sladb.nsf/sla/bm?Open")
				.contact("Praap A.K")
				.license("Apache License Version 2.0")
				.licenseUrl("https://github.com/IBM-Bluemix/news-aggregator/blob/master/LICENSE")
				.version("2.0")
				.build();
	}

	
	/*@Bean
	public CommandLineRunner demo(UserRepository repository, RolesRepository rolesRepository, PermissionRepository permissionRepository,
								  RolePermissionRepository rolePermissionRepository) {

		return (args) -> {
			
			User user = new User();
			user.setFirstName("Pratap");
			user.setLastName("AK");
			
			Address permanentAddress = new Address();
			permanentAddress.setCity("perm-Bangalore");
			permanentAddress.setPostBoxNumber("perm-123");
			permanentAddress.setState("perm-Karnataka");
			permanentAddress.setCountry("perm-India");
			permanentAddress.setStreet("perm-pratap road");
			
			Address temporaryAddress = new Address();
			temporaryAddress.setCity("temp-Bangalore");
			temporaryAddress.setPostBoxNumber("temp-123");
			temporaryAddress.setState("temp-Karnataka");
			temporaryAddress.setCountry("temp-India");
			temporaryAddress.setStreet("temp-pratap road");
			
			UserDetails uDetails = new UserDetails();
			uDetails.setFatherName("F");
			uDetails.setMotherName("M");
			uDetails.setGender("Male");
			uDetails.setMaritalStatus("Single");
			uDetails.setOccupation("Engg");
			uDetails.setNationality("Indian");
			uDetails.setReligion("Hindu");
			uDetails.setPermanentAddress(permanentAddress);
			uDetails.setTemporaryAddress(temporaryAddress);
			
			user.setUserDetails(uDetails);
			
			repository.save(user);

			*//*
			Defining roles for the application
			@TODO needs to find more specific roles as per the banking industry
			 *//*
			Roles role = new Roles();
			role.setRoleName("CLERK");
			rolesRepository.save(role);

			Roles role1 = new Roles();
			role1.setRoleName("MANAGER");
			rolesRepository.save(role1);

			Roles role2 = new Roles();
			role2.setRoleName("ACCOUNTANT");
			rolesRepository.save(role2);

			*//*
			Defining permissions for the application
			@TODO admin should able to change the permission through GUI
			 *//*
			Permissions permission = new Permissions();
			permission.setPermissionName("READ");
			permissionRepository.save(permission);

			Permissions permission1 = new Permissions();
			permission1.setPermissionName("READ_OR_WRITE");
			permissionRepository.save(permission1);

			Permissions permission2 = new Permissions();
			permission2.setPermissionName("DELETE");
			permissionRepository.save(permission2);

			*//*
			Assigning default permission to predefined roles
			@TODO admin should able to modify this temporarily or permanently
			 *//*
			Roles roleObj = rolesRepository.findByRoleId("1");
			Permissions permissionObj = permissionRepository.findByPermissionId("1");
			
			RolePermisson rolePermission = new RolePermisson();
			rolePermission.setRole(roleObj);
			rolePermission.setPermissions(permissionObj);
			rolePermissionRepository.save(rolePermission);			
					
			log.info("added user's successfully");			
		};
	}*/
}
