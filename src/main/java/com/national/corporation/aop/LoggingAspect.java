package com.national.corporation.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    private final Logger logger;

    public LoggingAspect() {
        logger = LoggerFactory.getLogger(getClass());
    }

    @Before("execution(public * com.national.corporation.Application.demo(..))")
    public void loggingAdvice() {
        logger.info("My first advice");
        logger.debug("debugger");
        logger.error("error");
        logger.trace("trace");
    }

}
