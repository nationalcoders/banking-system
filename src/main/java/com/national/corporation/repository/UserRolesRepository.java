package com.national.corporation.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.national.corporation.entity.Roles;
import com.national.corporation.entity.User;
import com.national.corporation.entity.UserRoles;

public interface UserRolesRepository extends CrudRepository<UserRoles, String> {
	
	List<UserRoles> findByUser(User user);
	
	List<UserRoles> findByRoles(Roles roles);
}
