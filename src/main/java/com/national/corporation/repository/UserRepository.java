package com.national.corporation.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.national.corporation.entity.User;

public interface UserRepository extends CrudRepository<User, Long>{
	
	List<User> findByLastName(String lastName);

}
