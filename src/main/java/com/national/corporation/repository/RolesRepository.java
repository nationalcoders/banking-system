package com.national.corporation.repository;

import org.springframework.data.repository.CrudRepository;

import com.national.corporation.entity.Roles;

public interface RolesRepository extends CrudRepository<Roles, String> {
	
	Roles findByRoleId(String roleId);

}
