package com.national.corporation.repository;

import org.springframework.data.repository.CrudRepository;

import com.national.corporation.entity.Permissions;

public interface PermissionRepository extends CrudRepository<Permissions, String>{
	
	Permissions findByPermissionId(String permissionId);

}
