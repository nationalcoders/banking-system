package com.national.corporation.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.national.corporation.entity.Permissions;
import com.national.corporation.entity.RolePermisson;
import com.national.corporation.entity.Roles;

public interface RolePermissionRepository extends CrudRepository<RolePermisson, String> {
	
	List<RolePermisson> findByRole(Roles role);
	
	List<RolePermisson> findByPermissions(Permissions permissions);

}
