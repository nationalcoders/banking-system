package com.national.corporation.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "IMPL_ROLE_PERMISSIONS")
public class RolePermisson {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private String id;
	
	@OneToOne
	@JoinColumn(name = "ROLE_ID")
	private Roles role;
	
	@OneToOne
	@JoinColumn(name = "PERMISSION_ID")
	private Permissions permissions;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}	

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Permissions getPermissions() {
		return permissions;
	}

	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

}
