package com.national.corporation.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IMPL_USER_ADDRESS")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;	
	
	@Column(name = "STREET_NAME")
	private String street;	
	
	@Column(name = "CITY_NAME")
	private String city;	
	
	@Column(name = "STATE_NAME")
	private String state;
	
	@Column(name = "COUNTRY_NAME")
	private String country;
	
	@Column(name = "POST_BOX_NUMBER")
	private String postBoxNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostBoxNumber() {
		return postBoxNumber;
	}

	public void setPostBoxNumber(String postBoxNumber) {
		this.postBoxNumber = postBoxNumber;
	}
	
	@Override
	public String toString() {
		return String.format("[street = %d]", street);
	}
}
